Creación de una publicación con docbook

Objetivo

Crear un manual, en pdf, de una aplicación (a elegir) docbook y las herramientas de conversión.
Descripción
- Se trata de elaborar un manual de al menos 10 páginas en pdf DIN A4.
- Tendrá capturas de pantalla, opciones de menú, listas, al menos una tabla, bibliografía o referencias utilizadas.
- Será un documento Docbook 5 válido.
- Se entregará el fuente de docbook (archivo o archivos xml) y todos los recursos usados (imágenes, capturas de pantalla, etc.)
- Se indicará el procedimiento utilizado en las conversiones (Windows o Linux)
- Se entregará la salida de los manuales en un html, pdf en DIN A4
- El manual contendrá un apéndice describiendo todo el proceso seguido para la elaboración del manual (herramientas utilizadas, dónde se ha recogido la información, etc.)
- Se entregará todo mediante un proyecto en bitbucket.

